﻿using StackExchange.Profiling;
using StackExchange.Profiling.Data;
using StudioKit.Repository.Interfaces;
using System;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlClient;

namespace StudioKit.TransientFaultHandling
{
	/// <summary>
	///     A DataContextFactory which wraps the inner SQLConnection with a
	///     <see cref="T:StudioKit.Cloud.TransientFaultHandling.ReliableDbConnection" />
	///     and a <see cref="T:StackExchange.Profiling.Data.ProfiledDbConnection" />.
	/// </summary>
	/// <typeparam name="TDataContext"></typeparam>
	public class ReliableDataContextFactory<TDataContext> : IDataContextFactory where TDataContext : DataContext, new()
	{
		private readonly String _connectionString;
		private TDataContext _context;

		/// <summary>
		///     Constructor for DI
		/// </summary>
		/// <param name="connectionString">The connection string for this factory</param>
		public ReliableDataContextFactory(String connectionString)
		{
			_connectionString = connectionString;
		}

		public void Dispose()
		{
			if (_context != null)
				_context.Dispose();
		}

		/// <summary>
		///     Factory method for getting the data context
		/// </summary>
		/// <returns>A data context</returns>
		public DataContext Context
		{
			get
			{
				if (_context != null)
					return _context;

				DbConnection connection;

				if (MiniProfiler.Current != null)
				{
					connection = new ProfiledDbConnection(
							new ReliableDbConnection(new SqlConnection(_connectionString)), MiniProfiler.Current);
				}
				else
				{
					connection = new ReliableDbConnection(new SqlConnection(_connectionString));
				}

				_context = (TDataContext)Activator.CreateInstance(typeof(TDataContext), connection);
				return _context;
			}
		}
	}
}