﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.TransientFaultHandling.Extensions;
using System.Data;
using System.Data.Common;

namespace StudioKit.TransientFaultHandling
{
	public class ReliableDbCommand : DbCommand
	{
		private readonly DbCommand _command;
		private DbConnection _connection;
		private DbTransaction _transaction;

		public ReliableDbCommand(DbCommand command, DbConnection connection)
		{
			_command = command;
			_connection = connection;
		}

		public override void Prepare()
		{
			_command.Prepare();
		}

		public override string CommandText
		{
			get { return _command.CommandText; }
			set { _command.CommandText = value; }
		}

		public override int CommandTimeout
		{
			get { return _command.CommandTimeout; }
			set { _command.CommandTimeout = value; }
		}

		public override CommandType CommandType
		{
			get { return _command.CommandType; }
			set { _command.CommandType = value; }
		}

		public override UpdateRowSource UpdatedRowSource
		{
			get { return _command.UpdatedRowSource; }
			set { _command.UpdatedRowSource = value; }
		}

		protected override DbConnection DbConnection
		{
			get
			{
				return _connection;
			}
			set
			{
				_connection = value;
				_command.Connection = _connection;
			}
		}

		protected override DbParameterCollection DbParameterCollection
		{
			get { return _command.Parameters; }
		}

		protected override DbTransaction DbTransaction
		{
			get { return _transaction; }
			set
			{
				_transaction = value;
				_command.Transaction = value;
			}
		}

		public override bool DesignTimeVisible
		{
			get { return _command.DesignTimeVisible; }
			set { _command.DesignTimeVisible = value; }
		}

		public override void Cancel()
		{
			_command.Cancel();
		}

		protected override DbParameter CreateDbParameter()
		{
			return _command.CreateParameter();
		}

		protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
		{
			return RetryManager.Instance.ExecuteSqlCommandWithRetry(() => _command.ExecuteReader(behavior));
		}

		public override int ExecuteNonQuery()
		{
			return RetryManager.Instance.ExecuteSqlCommandWithRetry(() => _command.ExecuteNonQuery());
		}

		public override object ExecuteScalar()
		{
			return RetryManager.Instance.ExecuteSqlCommandWithRetry(() => _command.ExecuteScalar());
		}
	}
}