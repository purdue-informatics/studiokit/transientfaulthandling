﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("StudioKit.TransientFaultHandling")]
[assembly: AssemblyDescription("A library to facilitate handling transient faults, such as those that occur using Azure SQL Databases and other remote services.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Purdue University ")]
[assembly: AssemblyProduct("StudioKit.TransientFaultHandling")]
[assembly: AssemblyCopyright("Copyright © Purdue University 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("41f79b4f-c144-4156-8451-a924fed0c4ca")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]