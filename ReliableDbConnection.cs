﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.TransientFaultHandling.ErrorDetectionStrategies;
using StudioKit.TransientFaultHandling.Extensions;
using System;
using System.Data;
using System.Data.Common;
using System.Transactions;
using IsolationLevel = System.Data.IsolationLevel;

namespace StudioKit.TransientFaultHandling
{
	/// <summary>
	/// Wraps a database connection and subclasses DbConnection. Uses RetryPolicy to Open InnerConnection.
	/// Adds Timeouts to TransientErrorDetection (for default constructor values).
	/// Alternative to <see cref="T:Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling.ReliableSqlConnection"/>.
	/// </summary>
	public class ReliableDbConnection : DbConnection, ICloneable
	{
		private DbConnection _connection;
		private readonly RetryPolicy _connectionRetryPolicy;
		private readonly RetryPolicy _commandRetryPolicy;
		private readonly RetryPolicy _connectionStringFailoverPolicy;

		public ReliableDbConnection(IDbConnection connection)
				: this(connection, RetryManager.Instance.GetDefaultSqlConnectionRetryPolicyWithTimeouts())
		{
		}

		public ReliableDbConnection(IDbConnection connection, RetryPolicy retryPolicy)
				: this(connection, retryPolicy, RetryManager.Instance.GetDefaultSqlCommandRetryPolicyWithTimeouts() ?? retryPolicy)
		{
		}

		public ReliableDbConnection(IDbConnection connection, RetryPolicy connectionRetryPolicy, RetryPolicy commandRetryPolicy)
		{
			if (connection == null)
				throw new ArgumentNullException("connection");
			_connection = (DbConnection)connection;
			_connection.StateChange += StateChangeHandler;
			_connectionRetryPolicy = connectionRetryPolicy;
			_commandRetryPolicy = commandRetryPolicy;
			_connectionStringFailoverPolicy = new RetryPolicy<NetworkConnectivityErrorDetectionStrategy>(1, TimeSpan.FromMilliseconds(1.0));
		}

		public RetryPolicy ConnectionRetryPolicy
		{
			get
			{
				return _connectionRetryPolicy;
			}
		}

		public RetryPolicy CommandRetryPolicy
		{
			get
			{
				return _commandRetryPolicy;
			}
		}

		public DbConnection InnerConnection
		{
			get
			{
				return _connection;
			}
		}

		#region DbConnection Overrides

		public override string ConnectionString
		{
			get
			{
				return _connection.ConnectionString;
			}
			set
			{
				_connection.ConnectionString = value;
			}
		}

		public override int ConnectionTimeout
		{
			get
			{
				return _connection.ConnectionTimeout;
			}
		}

		public override string Database
		{
			get
			{
				return _connection.Database;
			}
		}

		public override string DataSource
		{
			get
			{
				return _connection.DataSource;
			}
		}

		public override string ServerVersion
		{
			get
			{
				return _connection.ServerVersion;
			}
		}

		public override ConnectionState State
		{
			get
			{
				return _connection.State;
			}
		}

		public override void ChangeDatabase(string databaseName)
		{
			_connection.ChangeDatabase(databaseName);
		}

		public override void Close()
		{
			_connection.Close();
		}

		public override void EnlistTransaction(Transaction transaction)
		{
			_connection.EnlistTransaction(transaction);
		}

		public override DataTable GetSchema()
		{
			return
					(ConnectionRetryPolicy ?? RetryPolicy.NoRetry).ExecuteAction(() =>
							_connectionStringFailoverPolicy.ExecuteAction(() =>
									_connection.GetSchema()));
		}

		public override DataTable GetSchema(string collectionName)
		{
			return
					(ConnectionRetryPolicy ?? RetryPolicy.NoRetry).ExecuteAction(() =>
							_connectionStringFailoverPolicy.ExecuteAction(() =>
									_connection.GetSchema(collectionName)));
		}

		public override DataTable GetSchema(string collectionName, string[] restrictionValues)
		{
			return
					(ConnectionRetryPolicy ?? RetryPolicy.NoRetry).ExecuteAction(() =>
							_connectionStringFailoverPolicy.ExecuteAction(() =>
									_connection.GetSchema(collectionName, restrictionValues)));
		}

		public DbConnection Open(RetryPolicy retryPolicy)
		{
			(retryPolicy ?? RetryPolicy.NoRetry).ExecuteAction(() => _connectionStringFailoverPolicy.ExecuteAction(() =>
			{
				if (_connection.State == ConnectionState.Open)
					return;
				_connection.Open();
			}));
			return _connection;
		}

		public override void Open()
		{
			Open(ConnectionRetryPolicy);
		}

		protected override DbCommand CreateDbCommand()
		{
			return new ReliableDbCommand(_connection.CreateCommand(), this);
		}

		protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
		{
			return _connection.BeginTransaction(isolationLevel);
		}

		#endregion DbConnection Overrides

		protected override void Dispose(bool disposing)
		{
			if (disposing && _connection != null)
			{
				_connection.StateChange -= StateChangeHandler;
				_connection.Dispose();
			}
			_connection = null;
			base.Dispose(disposing);
		}

		private void StateChangeHandler(object sender, StateChangeEventArgs stateChangeEventArguments)
		{
			OnStateChange(stateChangeEventArguments);
		}

		public object Clone()
		{
			var cloneable = _connection as ICloneable;
			if (cloneable == null)
				throw new NotSupportedException("Underlying " + _connection.GetType().Name + " is not cloneable");
			return new ReliableDbConnection((DbConnection)cloneable.Clone(), _connectionRetryPolicy, _commandRetryPolicy);
		}

		object ICloneable.Clone()
		{
			return Clone();
		}
	}
}