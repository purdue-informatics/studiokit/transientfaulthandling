﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;

namespace StudioKit.TransientFaultHandling.Extensions
{
	public static class RetryManagerExtensions
	{
		public static RetryPolicy GetRetryPolicy<T>(this RetryManager retryManager, RetryStrategy retryStrategy) where T : ITransientErrorDetectionStrategy, new()
		{
			if (retryManager == null)
				throw new ArgumentNullException("retryManager");
			return retryManager.GetRetryPolicy<T>(retryStrategy.Name);
		}
	}
}