﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.TransientFaultHandling.RetryStrategies;
using System;

namespace StudioKit.TransientFaultHandling.Extensions
{
	public static class RetryManagerSqlExtenstions
	{
		public static RetryPolicy GetDefaultSqlCommandRetryPolicyWithTimeouts(this RetryManager retryManager)
		{
			if (retryManager == null)
				throw new ArgumentNullException("retryManager");
			return retryManager.GetRetryPolicy<SqlDatabaseTransientErrorDetectionStrategyWithTimeouts>(retryManager.GetDefaultSqlCommandRetryStrategy());
		}

		public static RetryPolicy GetDefaultSqlConnectionRetryPolicyWithTimeouts(this RetryManager retryManager)
		{
			if (retryManager == null)
				throw new ArgumentNullException("retryManager");
			return retryManager.GetRetryPolicy<SqlDatabaseTransientErrorDetectionStrategyWithTimeouts>(retryManager.GetDefaultSqlConnectionRetryStrategy());
		}

		public static T ExecuteSqlCommandWithRetry<T>(this RetryManager retryManager, Func<T> action)
		{
			try
			{
				return retryManager.GetDefaultSqlCommandRetryPolicyWithTimeouts().ExecuteAction(action);
			}
			catch (Exception ex)
			{
				HandleRetryException(retryManager, ex);
				throw;
			}
		}

		public static void ExecuteSqlCommandWithRetry(this RetryManager retryManager, Action action)
		{
			try
			{
				retryManager.GetDefaultSqlCommandRetryPolicyWithTimeouts().ExecuteAction(action);
			}
			catch (Exception ex)
			{
				HandleRetryException(retryManager, ex);
				throw;
			}
		}

		private static void HandleRetryException(RetryManager retryManager, Exception ex)
		{
			var loggingRetryManager = retryManager as LoggingRetryManager;
			if (loggingRetryManager == null || loggingRetryManager.LogFunction == null) return;
			loggingRetryManager.LogFunction(ex);
		}
	}
}