﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;

namespace StudioKit.TransientFaultHandling
{
	public static class TransientFaultHandling
	{
		/// <summary>
		/// Set a default RetryManager of type T with default retry strategies for SQL Connections, SQL Commands, Caching, Azure Storage and Service Bus.
		/// If T is LoggingRetryManager, set logFunction.
		/// Default retry strategy is "incremental".
		/// </summary>
		public static void ConfigureDefaultRetryManager<T>(Action<Exception> logFunction = null) where T : RetryManager
		{
			var fixedIntervalStrategy = new FixedInterval("fixed", 10, TimeSpan.FromSeconds(1));
			var backoffStrategy = new ExponentialBackoff("backoff", 10, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(1));
			var incrementalStrategy = new Incremental("incremental", 10, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
			var retryStrategies = new List<RetryStrategy>
						{
								fixedIntervalStrategy,
								backoffStrategy,
								incrementalStrategy
						};
			var defaultRetryStrategyNamesMap = new Dictionary<string, string>
						{
								{RetryManagerSqlExtensions.DefaultStrategyConnectionTechnologyName, "incremental"},
								{RetryManagerSqlExtensions.DefaultStrategyCommandTechnologyName, "backoff"},
								{RetryManagerCachingExtensions.DefaultStrategyTechnologyName, "fixed"},
								{RetryManagerWindowsAzureStorageExtensions.DefaultStrategyTechnologyName, "fixed"},
								{RetryManagerServiceBusExtensions.DefaultStrategyTechnologyName, "fixed"}
						};

			// create manager with all strategies, default strategy, and defaults for each technology
			var retryManager = (T)Activator.CreateInstance(typeof(T), retryStrategies, "incremental", defaultRetryStrategyNamesMap);

			// add logging if applicable
			var loggingRetryManager = retryManager as LoggingRetryManager;
			if (loggingRetryManager != null && logFunction != null)
				loggingRetryManager.LogFunction = logFunction;

			RetryManager.SetDefault(retryManager);
		}
	}
}