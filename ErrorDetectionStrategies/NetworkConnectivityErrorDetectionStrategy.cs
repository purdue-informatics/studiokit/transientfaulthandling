﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Data.SqlClient;

namespace StudioKit.TransientFaultHandling.ErrorDetectionStrategies
{
	public class NetworkConnectivityErrorDetectionStrategy : ITransientErrorDetectionStrategy
	{
		public bool IsTransient(Exception ex)
		{
			SqlException sqlException;
			return ex != null && (sqlException = ex as SqlException) != null && sqlException.Number == 11001;
		}
	}
}